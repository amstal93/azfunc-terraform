output "azfunc_name" {
  value = azurerm_function_app.this.name
}

output "azfunc_url" {
  value = azurerm_function_app.this.default_hostname
}