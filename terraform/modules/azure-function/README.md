# Examples
```
# Put below in versions.tf file
terraform {
  backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.65"
    }
  }
  required_version = ">= 1.0.0"
}

# Put below in provider.tf file
provider "azurerm" {
  features {}
}

# Put below in variables.tf file
variable "group_name" {
  type = string
  default = "azfunct-20210706"
}

variable "location" {
  type = string
  default = "uksouth"
}

variable "stage" {
  type = string
  default = "local"
}

# Put below in outputs.tf
output "primary_name" {
  value = module.primary.azfunc_name
}

output "secondary_name" {
  value = module.secondary.azfunc_name
}

# Start of main.tf
resource "random_id" "id" {
  byte_length = 4
}

resource "azurerm_resource_group" "this" {
  name     = var.group_name
  location = var.location
}

resource "azurerm_storage_account" "this" {
  name                      = "${var.stage}${random_id.id.hex}"
  resource_group_name       = azurerm_resource_group.this.name
  location                  = var.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
}

module "primary" {
 #source = "git::<repo-url>?ref=<tag>"
  source = "../../modules/azure-function"

  stage               = var.stage
  location            = var.location
  function_name       = "primary"
  os                  = "linux"
  resource_group_name = azurerm_resource_group.this.name
  storage_account = {
    name = "${var.stage}${random_id.id.hex}"
    key  = azurerm_storage_account.this.primary_access_key
  }
  app_service_plan_config = {
    kind = "Linux",
    tier = "Dynamic",
    size = "Y1"
  }
}

module "secondary" {
 #source = "git::<repo-url>?ref=<tag>"
  source = "../../modules/azure-function"

  stage               = var.stage
  location            = var.location
  function_name       = "secondary"
  os                  = "windows"
  resource_group_name = azurerm_resource_group.this.name
  storage_account = {
    name = "${var.stage}${random_id.id.hex}"
    key  = azurerm_storage_account.this.primary_access_key
  }
  app_service_plan_config = {
    kind = "FunctionApp",
    tier = "Dynamic",
    size = "Y1"
  }
}
```
# Related documentation
* [Terraform resource documentation](www.terraform.io/docs/providers/azurerm/r/app\_service\_plan.html)
* [Microsoft Azure documentation](docs.microsoft.com/en-us/azure/app-service/overview-hosting-plans)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | ~>2.66 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | ~>2.66 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_app_service_plan.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/app_service_plan) | resource |
| [azurerm_application_insights.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/application_insights) | resource |
| [azurerm_function_app.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/function_app) | resource |
| [random_id.id](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_service_plan_config"></a> [app\_service\_plan\_config](#input\_app\_service\_plan\_config) | n/a | `map` | <pre>{<br>  "kind": "Linux",<br>  "size": "Y1",<br>  "tier": "Dynamic"<br>}</pre> | no |
| <a name="input_builtin_logging"></a> [builtin\_logging](#input\_builtin\_logging) | n/a | `bool` | `true` | no |
| <a name="input_function_name"></a> [function\_name](#input\_function\_name) | n/a | `string` | `null` | no |
| <a name="input_location"></a> [location](#input\_location) | n/a | `string` | `null` | no |
| <a name="input_os"></a> [os](#input\_os) | OS of function app. Must be 'linux' or 'windows' | `string` | `null` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | n/a | `string` | `null` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | n/a | `string` | `null` | no |
| <a name="input_storage_account"></a> [storage\_account](#input\_storage\_account) | n/a | `map` | <pre>{<br>  "key": null,<br>  "name": null<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_azfunc_name"></a> [azfunc\_name](#output\_azfunc\_name) | n/a |
