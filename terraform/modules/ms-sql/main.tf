/**
  * # Example
  * ```
  *  module "ms_sql" {
  *      source = "git::<repo-url>?ref=<tag>"
  *  
  *      stage = "dev"
  *      location = "westeurope"
  *      ms_sql_server = {
  *          "name" = "ac-ms-sql",
  *          "administrator_login" = "admin",
  *          "version" = "12.0",
  *          "public_network_access_enabled" = true,
  *          "max_size_gb" = 250
  *      }
  *      ms_sql_databases = {
  *          "default" = {     
  *              pitr_days = 35,              # Optional:Trasactional log retention period
  *              max_size_gb = 250,
  *              sku_name = "S1",
  *              storage_account_type = "GRS",
  *              monthly_retention = "P1Y",   # Optional:Retain monthly backups for 1 year
  *              yearly_retention = "P5Y",    # Optional:yearly backups for 5 years
  *              week_of_year = 25            # Optional:yearly backup taken from this week
  *
  *          },
  *          "shortTermBackupOnly" = {       
  *              pitr_days = 7,
  *              max_size_gb = 250,
  *              sku_name = "S1",
  *              storage_account_type = "GRS"
  *          }
  *      }
  *      firewall_cidr = {
  *          "Office Public IPv4" = {
  *              end_ip_address   = "8.8.8.8"
  *              start_ip_address = "8.8.8.8"
  *          },
  *          "AllowAllWindowsAzureIps" = {      
  *              end_ip_address   = "0.0.0.0"
  *              start_ip_address = "0.0.0.0"
  *          }
  *      }
  *      administrator_login_password = Use a secret.tfvars or cli option `-vars` to securely add secret values
  *      resource_group_name = "my-test"
  *  }
  * ```
*/

resource "azurerm_resource_group" "this" {
  location = var.location
  name     = coalesce(var.resource_group_name, "ms-sql-${var.stage}")
}

resource "azurerm_mssql_server" "this" {
  administrator_login           = var.ms_sql_server.administrator_login
  administrator_login_password  = var.administrator_login_password
  connection_policy             = "Default"
  location                      = var.location
  name                          = var.ms_sql_server.name #Globally unique
  public_network_access_enabled = var.ms_sql_server.public_network_access_enabled
  resource_group_name           = azurerm_resource_group.this.name
  version                       = var.ms_sql_server.version

  lifecycle {
    #prevent_destroy = true
    ignore_changes = [
      administrator_login_password,
    ]
  }
}

# Supports multiple databases per server
resource "azurerm_mssql_database" "this" {
  for_each = var.ms_sql_databases

  auto_pause_delay_in_minutes = 0
  collation                   = "SQL_Latin1_General_CP1_CI_AS"
  create_mode                 = "Default"
  geo_backup_enabled          = true
  max_size_gb                 = each.value["max_size_gb"]
  min_capacity                = 0
  name                        = each.key
  read_replica_count          = 0
  read_scale                  = false
  server_id                   = azurerm_mssql_server.this.id
  sku_name                    = each.value["sku_name"]
  storage_account_type        = each.value["storage_account_type"]
  zone_redundant              = false

  short_term_retention_policy {
    retention_days = try(each.value["pitr_days"], 7)
  }

  # See https://github.com/terraform-providers/terraform-provider-azurerm/issues/9067
  # Result is an "in-place upgrade" tf action in every plan
  long_term_retention_policy {
    monthly_retention = try(each.value["monthly_retention"], "PT0S")
    yearly_retention  = try(each.value["yearly_retention"], "PT0S")
    week_of_year      = try(each.value["week_of_year"], 1)
  }

  threat_detection_policy {
    disabled_alerts      = []
    email_account_admins = "Disabled"
    email_addresses      = []
    retention_days       = 0
    state                = "Disabled"
    use_server_default   = "Disabled"
  }
}

resource "azurerm_mssql_firewall_rule" "this" {
  for_each = var.firewall_cidr

  end_ip_address   = each.value["end_ip_address"]
  name             = each.key
  server_id        = azurerm_mssql_server.this.id
  start_ip_address = each.value["start_ip_address"]
}

# Conditionally protect all Azure SQL resources. var.cannot_delete/read_only == 0|1
# NB. AzureRM locks will only stop terraform from destroying a resource if:
#   - The lock is not defined in the same config as the protected resource
#   - Terraform is not aware the protected resource is dependent on the lock, 
#     therefore doesn't remove the lock before attempting to destroy the protected resource
resource "azurerm_management_lock" "resource_group_cannot_delete" {
  count      = var.resource_group_cannot_delete
  name       = "lock-${azurerm_resource_group.this.name}"
  scope      = azurerm_resource_group.this.id
  lock_level = "CanNotDelete"
  notes      = "Protect the SQL DB backups which will be destroyed if the SQL Server resource is deleted"

  lifecycle {
    prevent_destroy = true
  }
}

# Race condition: If lock applies immediately after azurerm_mssql_server resource is created then azurerm_mssql_server child resources will fail to create
# Fix - explicitly declare child resources of azurerm_mssql_server as azurerm_management_lock resource dependencies
resource "azurerm_management_lock" "sql_server_read_only" {
  count      = var.sql_server_read_only
  name       = "lock-${azurerm_mssql_server.this.name}"
  scope      = azurerm_mssql_server.this.id
  lock_level = "ReadOnly"
  notes      = "You require approval from the sysadmin team to edit this lock"

  lifecycle {
    prevent_destroy = true
  }

  depends_on = [
    azurerm_mssql_firewall_rule.this,
    azurerm_mssql_database.this,
  ]
}