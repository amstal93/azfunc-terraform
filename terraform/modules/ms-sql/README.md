# Example
```
 module "ms_sql" {
     source = "git::<repo-url>?ref=<tag>"

     stage = "dev"
     location = "westeurope"
     ms_sql_server = {
         "name" = "ac-ms-sql",
         "administrator_login" = "admin",
         "version" = "12.0",
         "public_network_access_enabled" = true,
         "max_size_gb" = 250
     }
     ms_sql_databases = {
         "default" = {     
             pitr_days = 35,
             max_size_gb = 250,
             sku_name = "S1",
             storage_account_type = "GRS",
             monthly_retention = "P1Y", #Retain monthly backups for 1 year
             yearly_retention = "P5Y", #Retain yearly backups for 5 years
             week_of_year = 25

         },
         "shortTermBackupOnly" = {       
             pitr_days = 7,
             max_size_gb = 250,
             sku_name = "S1",
             storage_account_type = "GRS",
             monthly_retention = "",
             yearly_retention = "",
             week_of_year = 0
         }
     }
     firewall_cidr = {
         "Office Public IPv4" = {
             end_ip_address   = "8.8.8.8"
             start_ip_address = "8.8.8.8"
         },
         "AllowAllWindowsAzureIps" = {      
             end_ip_address   = "0.0.0.0"
             start_ip_address = "0.0.0.0"
         }
     }
     administrator_login_password = Use a secret.tfvars or cli option `-vars` to securely add secret values
     resource_group_name = "my-test"
 }
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.00 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | ~>2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | ~>2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_mssql_database.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/mssql_database) | resource |
| [azurerm_mssql_firewall_rule.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/mssql_firewall_rule) | resource |
| [azurerm_mssql_server.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/mssql_server) | resource |
| [azurerm_resource_group.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_administrator_login_password"></a> [administrator\_login\_password](#input\_administrator\_login\_password) | n/a | `string` | n/a | yes |
| <a name="input_firewall_cidr"></a> [firewall\_cidr](#input\_firewall\_cidr) | n/a | `map` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | n/a | `string` | n/a | yes |
| <a name="input_ms_sql_databases"></a> [ms\_sql\_databases](#input\_ms\_sql\_databases) | n/a | `map` | n/a | yes |
| <a name="input_ms_sql_server"></a> [ms\_sql\_server](#input\_ms\_sql\_server) | n/a | `map` | n/a | yes |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | n/a | `string` | `""` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
