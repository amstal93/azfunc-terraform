# Notes

- Prevent deletion of SQL Server and databases. Using AzureRM Locks and tf Lifecylce policies
  - Deletion of SQL Server will also delete all database backups
  - Alternative: Create Maintenance Plan within db which uses Blob as destination
- Required fix: Long term backup should be optional
  - Currently requires tf update action each run because 0 (returned value) is not valid input for week_of_year 

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.00 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | ~>2.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ms_sql"></a> [ms\_sql](#module\_ms\_sql) | ../modules/ms-sql | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_administrator_login_password"></a> [administrator\_login\_password](#input\_administrator\_login\_password) | n/a | `string` | n/a | yes |
| <a name="input_firewall_cidr"></a> [firewall\_cidr](#input\_firewall\_cidr) | n/a | `map` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | n/a | `string` | n/a | yes |
| <a name="input_ms_sql_databases"></a> [ms\_sql\_databases](#input\_ms\_sql\_databases) | n/a | `map` | n/a | yes |
| <a name="input_ms_sql_server"></a> [ms\_sql\_server](#input\_ms\_sql\_server) | n/a | `map` | n/a | yes |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | n/a | `string` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
