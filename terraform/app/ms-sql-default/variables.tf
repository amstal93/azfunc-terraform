variable "stage" {
  type = string
}

variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "ms_sql_server" {
  type = map(any)
}

variable "ms_sql_databases" {
  type = map(any)
}

variable "firewall_cidr" {
  type = map(any)
}

variable "administrator_login_password" {
  type      = string
  sensitive = true
}

variable "resource_group_cannot_delete" {
  type    = number
}

variable "sql_server_read_only" {
  type    = number
}