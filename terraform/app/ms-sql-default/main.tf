module "ms_sql" {
  #source = "git::<repo-url>?ref=<tag>"
  source = "../../modules/ms-sql"

  stage         = var.stage
  location      = var.location
  resource_group_cannot_delete     = 1
  sql_server_read_only = 1

  administrator_login_password = var.administrator_login_password
  firewall_cidr                = var.firewall_cidr
  ms_sql_databases             = var.ms_sql_databases
  ms_sql_server                = var.ms_sql_server
  resource_group_name          = var.resource_group_name
}