variable "project_name" {
  type = string
}

variable "location" {
  type    = string
  default = "uksouth"
}

variable "stage" {
  type = string
}

variable "app_service_plan_config" {
  type = map(any)
}