resource "random_id" "id" {
  byte_length = 4
}

resource "azurerm_resource_group" "this" {
  name     = var.group_name
  location = var.location
}

resource "azurerm_storage_account" "this" {
  name                      = "${var.stage}${random_id.id.hex}"
  resource_group_name       = azurerm_resource_group.this.name
  location                  = var.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
}

resource "azurerm_storage_queue" "this" {
  name                 = "incoming"
  storage_account_name = azurerm_storage_account.this.name
}

module "primary" {
  #source = "git::<repo-url>?ref=<tag>"
  source = "../../modules/azure-function"

  stage               = var.stage
  location            = var.location
  function_name       = "primary"
  os                  = "linux"
  resource_group_name = azurerm_resource_group.this.name
  storage_account = {
    name = azurerm_storage_account.this.name
    key  = azurerm_storage_account.this.primary_access_key
  }
  app_service_plan_config = {
    kind = "FunctionApp",
    tier = "Dynamic",
    size = "Y1"
  }
}

# Windows option currently broken. Pending https://github.com/terraform-providers/terraform-provider-azurerm/issues/11859
# module "secondary" {
#  #source = "git::<repo-url>?ref=<tag>"
#   source = "../../modules/azure-function"
# 
#   stage               = var.stage
#   location            = var.location
#   function_name       = "secondary"
#   os                  = "windows"
#   resource_group_name = azurerm_resource_group.this.name
#   storage_account = {
#     name = "${var.stage}${random_id.id.hex}"
#     key  = azurerm_storage_account.this.primary_access_key
#   }
#  app_service_plan_config = {
#      kind = "FunctionApp",
#      tier = "Dynamic",
#      size = "Y1"
#  }
# }