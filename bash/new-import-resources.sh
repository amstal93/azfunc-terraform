#!/bin/bash
<< COMMENT
Creates App Service plan for use in terraform/app/import-app-service
Run from root of repo dir: ./bash/new-import-resources.sh
COMMENT

RESOURCE_GROUP_NAME="achdwick-import-rg"
WEBAPP_NAME="ac-import-$RANDOM" #between 3-24 char, alphanumeric only
CONTAINER_NAME="tfstate"
LOCATION=uksouth

# Create resource group
az group create --name $RESOURCE_GROUP_NAME --location $LOCATION

# Create App Service Plan, Linux app service and upload demo python webapp
cd $PWD/src/python-docs-hello-world
az webapp up --name $WEBAPP_NAME \
    --resource-group $RESOURCE_GROUP_NAME \
    --plan "${WEBAPP_NAME}-plan" \
    --location $LOCATION \
    --sku FREE