#!/bin/bash
<< COMMENT
Will use azure subscription configured by Azure CLI environment
COMMENT

RESOURCE_GROUP_NAME="app-config-rg"
LOCATION=uksouth
APP_CONFIG_NAME="azfunc-terraform${RANDOM}"

# Create resource group
az group create --name $RESOURCE_GROUP_NAME --location $LOCATION

# Create App Configuration instance
az appconfig create --resource-group $RESOURCE_GROUP_NAME --name $APP_CONFIG_NAME --location $LOCATION --sku Standard